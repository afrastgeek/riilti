<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php // display welcome message from admin settings ?>
<p class="display-4" style="width: 50rem;"><?php echo $welcome_message; ?></p>

<div class="row my-5">
    <div class="col">
        <a href="<?php echo base_url('property'); ?>" class="btn btn-lg btn-outline-primary">Find yours</a>
    </div>
</div>