<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $now = time(); ?>

<?php // data rows ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card" style="">
                <div class="card-header bg-white">
                    <p class="card-text"><?php echo sprintf(lang('properties text posted_on_with_detail'),timespan(strtotime($property['updated']), $now, 1), $property['updated']); ?></p>
                </div>
                <div class="card-img-top">
                    <img class="card-img-top img-fluid" src="<?php echo base_url() . (($property['picture'] != '') ? 'uploads/' . $property['picture'] : 'themes/core/img/house-placeholder.png'); ?>" >
                </div>
                <div class="card-block">
                    <div class="bg-faded">
                        <p class="p-3"><?php echo sprintf(lang('properties text property_type'), ucfirst($property['type'])); ?></p>
                    </div>
                    <p class="card-text"><?php echo $property['description']; ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card bg-success">
                <div class="card-block text-center text-white">
                    <p class="card-text h1">Rp <?php echo number_format($property['price'],0,',','.'); ?></p>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-block text-center">
                    <p class="card-text h2"><?php echo $property['owner']; ?></p>
                    <p class="card-text"><?php echo $property['phone']; ?></p>
                </div>
            </div>
            <div class="mt-3">
                <a href="<?php echo base_url() . "transaction/add/" . $property['id']; ?>" class="btn btn-primary btn-lg btn-block"><?php echo lang('properties button buy'); ?></a>
            </div>
            <div class="card mt-3">
                <div class="card-img-top google-maps">
                    <iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAjy037PyGJomHdU5_7HR9fYrcIIqTRRpw&q=<?php echo (($property['coordinate'] == '') ? $property['address'] : $property['coordinate']); ?>" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-block">
                    <p class="card-text"><?php echo $property['address']; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
