<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $now = time(); ?>

<?php // data rows ?>
<div class="container">
    <div class="row">
        <div class="col">
            <span class="mr-2">Sort by</span>
            <a href="<?php echo current_url(); ?>?sort=type&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>" class="btn btn-secondary">
                <?php echo lang('properties col type'); ?> <?php if ($sort == 'type') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
            </a>
            <a href="<?php echo current_url(); ?>?sort=address&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>" class="btn btn-secondary">
                <?php echo lang('properties col address'); ?> <?php if ($sort == 'address') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
            </a>
            <a href="<?php echo current_url(); ?>?sort=price&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>" class="btn btn-secondary">
                <?php echo lang('properties col price'); ?> <?php if ($sort == 'price') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
            </a>
            <a href="<?php echo $this_url; ?>" class="btn btn-outline-danger tooltips mb-2 mr-sm-2 mb-sm-0" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="fa fa-refresh"></span> <?php echo lang('core button reset'); ?></a>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col">
            <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters", 'class'=>"form-inline")); ?>
                <span class="mr-2">Filter by</span>

                <label class="sr-only" for="type"><?php echo lang('properties input type'); ?></label>
                <?php echo form_input(array('name'=>'type', 'id'=>'type', 'class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=>lang('properties input type'), 'value'=>set_value('type', ((isset($filters['type'])) ? $filters['type'] : '')))); ?>

                <label class="sr-only" for="address"><?php echo lang('properties input address'); ?></label>
                <?php echo form_input(array('name'=>'address', 'id'=>'address', 'class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=>lang('properties input address'), 'value'=>set_value('address', ((isset($filters['address'])) ? $filters['address'] : '')))); ?>

                <label class="sr-only" for="address"><?php echo lang('properties input price'); ?></label>
                <?php echo form_input(array('name'=>'price', 'id'=>'price', 'class'=>'form-control mb-2 mr-sm-2 mb-sm-0', 'placeholder'=>lang('properties input price'), 'value'=>set_value('price', ((isset($filters['price'])) ? $filters['price'] : '')))); ?>

                <a href="<?php echo $this_url; ?>" class="btn btn-outline-danger tooltips mb-2 mr-sm-2 mb-sm-0" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="fa fa-refresh"></span> <?php echo lang('core button reset'); ?></a>

                <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-outline-success tooltips mb-2 mr-sm-2 mb-sm-0" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="fa fa-filter"></span> <?php echo lang('core button filter'); ?></button>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div class="row mt-5">
        <?php if ($total) : ?>
            <?php $i = 0; ?>
            <?php foreach ($properties as $property) : ?>
            <div class="col-md-4">
                <a href="<?php echo base_url('property/detail/' . $property['id']); ?>" class="list__card">
                    <div class="card" id="property<?php echo $property['id']; ?>">
                        <img class="card-img-top img-fluid" src="<?php echo base_url() . (($property['picture'] != '') ? 'uploads/' . $property['picture'] : 'themes/core/img/house-placeholder.png'); ?>" >
                        <div class="card-block">
                            <p class="card-text">
                                <span class="font-weight-bold"><?php echo ucfirst($property['type']); ?></span>
                                <span class="text-success">Rp <?php echo number_format($property['price'],0,',','.'); ?></span>
                            </p>
                            <p class="card-text small" title="<?php echo $property['updated']; ?>"><?php echo sprintf(lang('properties text posted_on_by'), timespan(strtotime($property['updated']), $now, 1), $property['owner']); ?>
                        </div>
                    </div>
                </a>
            </div>
            <?php $i++; if ($i%3 == 0) echo '</div><div class="row mt-4">'; ?>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="col">
                <h1><?php echo lang('core error no_results'); ?></h1>
            </div>
        <?php endif; ?>
    </div>
</div>
