<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Property</th>
                        <th>Payment Code</th>
                        <th>Status</th>
                        <th>Last Updated</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($total) : ?>
                    <?php $i = 1; ?>
                    <?php foreach ($transactions as $transaction) : ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <a href="<?php echo base_url() . 'property/detail/' . $transaction['property_id']; ?>"><?php echo $transaction['property_id']; ?></a>
                                </td>
                            <td><?php echo $transaction['paycode']; ?></td>
                            <td><?php echo (($transaction['status'] == 0) ? 'Booked' : 'Paid'); ?></td>
                            <td><?php echo $transaction['updated']; ?></td>
                        </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="col">
                        <h1><?php echo lang('core error no_results'); ?></h1>
                    </div>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>