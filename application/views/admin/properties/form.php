<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open_multipart('', array('role'=>'form')); ?>

    <?php // hidden id ?>
    <?php if (isset($property_id)) : ?>
        <?php echo form_hidden('id', $property_id); ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12 col-md-6">

            <div class="row">
                <?php // user_id ?>
                <div class="form-group col-sm-8<?php echo form_error('user_id') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input user_id'), 'user_id', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'user_id', 'value'=>set_value('user_id', (isset($property['user_id']) ? $property['user_id'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

            <div class="row">
                <?php // type ?>
                <div class="form-group col<?php echo form_error('type') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input type'), 'type', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'type', 'value'=>set_value('type', (isset($property['type']) ? $property['type'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

            <div class="row">
                <?php // address ?>
                <div class="form-group col<?php echo form_error('address') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input address'), 'address', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'address', 'value'=>set_value('address', (isset($property['address']) ? $property['address'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

            <div class="row">
                <?php // coordinate ?>
                <div class="form-group col<?php echo form_error('coordinate') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input coordinate'), 'coordinate', array('class'=>'control-label')); ?>
                    <?php echo form_input(array('name'=>'coordinate', 'value'=>set_value('coordinate', (isset($property['coordinate']) ? $property['coordinate'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

            <div class="row">
                <?php // phone ?>
                <div class="form-group col-sm-6<?php echo form_error('phone') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input phone'), 'phone', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'phone', 'type' => 'tel', 'value'=>set_value('phone', (isset($property['phone']) ? $property['phone'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

            <div class="row">
                <?php // price ?>
                <div class="form-group col-sm-6<?php echo form_error('price') ? ' has-error' : ''; ?>">
                    <?php echo form_label(lang('properties input price'), 'price', array('class'=>'control-label')); ?>
                    <span class="required">*</span>
                    <?php echo form_input(array('name'=>'price', 'type' => 'number', 'value'=>set_value('price', (isset($property['price']) ? $property['price'] : '')), 'class'=>'form-control')); ?>
                </div>
            </div>

        </div>

        <div class="col-sm-12 col-md-6">

            <label class="control-label"><?php echo lang('properties input image'); ?></label><br>
            <label class="custom-file">
                <input type="file" id="file" class="custom-file-input" name="picture">
                <span class="custom-file-control"></span>
            </label>

            <div class="row">
                <div class="col-sm-6 col-md-10">
                    <img id="selected-image" class="img-thumbnail hidden-xs-up" src="#">
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <?php // description ?>
        <div class="form-group col-sm-6<?php echo form_error('description') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('properties input description'), 'description', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_textarea(array('name'=>'description', 'value'=>set_value('description', (isset($property['description']) ? $property['description'] : '')), 'class'=>'form-control')); ?>
        </div>

        <?php // status ?>
        <div class="form-group col-sm-3<?php echo form_error('status') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('properties input status'), '', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'value'=>'1', 'checked'=>(( ! isset($property['status']) OR (isset($property['status']) && (int)$property['status'] == 1) OR $property['id'] == 1) ? 'checked' : FALSE))); ?>
                    <?php echo lang('admin input active'); ?>
                </label>
            </div>
            <?php if ( ! $property['id'] OR $property['id'] > 1) : ?>
                <div class="radio">
                    <label>
                        <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'value'=>'0', 'checked'=>((isset($property['status']) && (int)$property['status'] == 0) ? 'checked' : FALSE))); ?>
                        <?php echo lang('admin input inactive'); ?>
                    </label>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php // buttons ?>
    <div class="row">
        <span class="mr-auto"></span> <!-- workaround to replace .pull-right on row which made element overlapped by footer -->
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-success"><span class="fa fa-save"></span> <?php echo lang('core button save'); ?></button>
    </div>

<?php echo form_close(); ?>
