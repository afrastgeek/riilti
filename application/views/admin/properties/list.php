<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3 class="card-title"><?php echo lang('properties title property_list'); ?></h3>
            </div>
            <div class="col-md-6 text-right">
                <a class="btn btn-success tooltips" href="<?php echo base_url('admin/properties/add'); ?>" title="<?php echo lang('properties tooltip add_new_property') ?>" data-toggle="tooltip"><span class="fa fa-plus"></span> <?php echo lang('properties button add_new_property'); ?></a>
            </div>
        </div>
    </div>

    <table class="table table-striped table-hover-warning mb-0 table-responsive">
        <thead>

            <?php // sortable headers ?>
            <tr>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col property_id'); ?></a>
                    <?php if ($sort == 'id') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=user_id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col owner'); ?></a>
                    <?php if ($sort == 'user_id') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=type&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col type'); ?></a>
                    <?php if ($sort == 'type') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=address&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col address'); ?></a>
                    <?php if ($sort == 'address') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=price&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col price'); ?></a>
                    <?php if ($sort == 'price') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=phone&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('properties col phone'); ?></a>
                    <?php if ($sort == 'phone') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=status&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('admin col status'); ?></a>
                    <?php if ($sort == 'status') : ?><span class="fa fa-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td class="pull-right">
                    <?php echo lang('admin col actions'); ?>
                </td>
            </tr>

            <?php // search filters ?>
            <tr>
                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                    <th>
                    </th>
                    <th<?php echo ((isset($filters['user_id'])) ? ' class="has-success"' : ''); ?>>
                        <?php echo form_input(array('name'=>'user_id', 'id'=>'user_id', 'class'=>'form-control input-sm', 'placeholder'=>lang('properties input user_id'), 'value'=>set_value('user_id', ((isset($filters['user_id'])) ? $filters['user_id'] : '')))); ?>
                    </th>
                    <th<?php echo ((isset($filters['type'])) ? ' class="has-success"' : ''); ?>>
                        <?php echo form_input(array('name'=>'type', 'id'=>'type', 'class'=>'form-control input-sm', 'placeholder'=>lang('properties input type'), 'value'=>set_value('type', ((isset($filters['type'])) ? $filters['type'] : '')))); ?>
                    </th>
                    <th<?php echo ((isset($filters['address'])) ? ' class="has-success"' : ''); ?>>
                        <?php echo form_input(array('name'=>'address', 'id'=>'address', 'class'=>'form-control input-sm', 'placeholder'=>lang('properties input address'), 'value'=>set_value('address', ((isset($filters['address'])) ? $filters['address'] : '')))); ?>
                    </th>
                    <th<?php echo ((isset($filters['price'])) ? ' class="has-success"' : ''); ?>>
                        <?php echo form_input(array('name'=>'price', 'id'=>'price', 'class'=>'form-control input-sm', 'placeholder'=>lang('properties input price'), 'value'=>set_value('price', ((isset($filters['price'])) ? $filters['price'] : '')))); ?>
                    </th>
                    <th<?php echo ((isset($filters['phone'])) ? ' class="has-success"' : ''); ?>>
                        <?php echo form_input(array('name'=>'phone', 'id'=>'phone', 'class'=>'form-control input-sm', 'placeholder'=>lang('properties input phone'), 'value'=>set_value('phone', ((isset($filters['phone'])) ? $filters['phone'] : '')))); ?>
                    </th>
                    <th colspan="2">
                        <div class="text-right" style="min-width:11.5rem;">
                            <a href="<?php echo $this_url; ?>" class="btn btn-danger tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="fa fa-refresh"></span> <?php echo lang('core button reset'); ?></a>
                            <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="fa fa-filter"></span> <?php echo lang('core button filter'); ?></button>
                        </div>
                    </th>
                <?php echo form_close(); ?>
            </tr>

        </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($properties as $property) : ?>
                    <tr>
                        <td<?php echo (($sort == 'id') ? ' class="sorted"' : ''); ?>>
                            <?php echo $property['id']; ?>
                        </td>
                        <td<?php echo (($sort == 'user_id') ? ' class="sorted"' : ''); ?>>
                            <?php echo $property['owner'] . " (" . $property['user_id'] . ")"; ?>
                        </td>
                        <td<?php echo (($sort == 'type') ? ' class="sorted"' : ''); ?>>
                            <?php echo ucfirst($property['type']); ?>
                        </td>
                        <td<?php echo (($sort == 'address') ? ' class="sorted"' : ''); ?>>
                            <?php echo $property['address']; ?>
                        </td>
                        <td class="text-right<?php echo (($sort == 'price') ? ' sorted' : ''); ?>">
                            <?php echo number_format($property['price'],2,',','.'); ?>
                        </td>
                        <td<?php echo (($sort == 'phone') ? ' class="sorted"' : ''); ?>>
                            <?php echo $property['phone']; ?>
                        </td>
                        <td<?php echo (($sort == 'status') ? ' class="sorted"' : ''); ?>>
                            <?php echo ($property['status']) ? '<span class="active">' . lang('admin input active') . '</span>' : '<span class="inactive">' . lang('admin input inactive') . '</span>'; ?>
                        </td>
                        <td>
                            <div class="text-right">
                                <div class="btn-group">
                                    <a href="#modal-<?php echo $property['id']; ?>" data-toggle="modal" class="btn btn-danger" title="<?php echo lang('admin button delete'); ?>"><span class="fa fa-trash"></span></a>
                                    <a href="<?php echo $this_url; ?>/edit/<?php echo $property['id']; ?>" class="btn btn-warning" title="<?php echo lang('admin button edit'); ?>"><span class="fa fa-pencil"></span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

        </tbody>
    </table>

    <?php // list tools ?>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-2 text-left">
                <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
            </div>
            <div class="col-md-2 text-left">
                <?php if ($total > 10) : ?>
                    <select id="limit" class="form-control">
                        <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                        <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                    </select>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php echo $pagination; ?>
            </div>
            <div class="col-md-2 text-right">
                <?php if ($total) : ?>
                    <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="fa fa-level-up"></span> <?php echo lang('admin button csv_export'); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>

<?php // delete modal ?>
<?php if ($total) : ?>
    <?php foreach ($properties as $property) : ?>
        <div class="modal fade" id="modal-<?php echo $property['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $property['id']; ?>" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 id="modal-label-<?php echo $property['id']; ?>"><?php echo lang('properties title property_delete');  ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo sprintf(lang('properties msg delete_confirm'), $property['address'] . " " . $property['coordinate']); ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                        <button type="button" class="btn btn-primary btn-delete-property" data-id="<?php echo $property['id']; ?>"><?php echo lang('admin button delete'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
