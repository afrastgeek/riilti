<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php echo form_open('', array('role'=>'form')); ?>

    <div class="row">
        <?php // paycode ?>
        <div class="form-group col-sm-4<?php echo form_error('paycode') ? ' has-error' : ''; ?>">
            <?php echo form_label(lang('transactions input paycode'), 'paycode', array('class'=>'control-label')); ?>
            <span class="required">*</span>
            <?php echo form_input(array('name'=>'paycode', 'value'=>set_value('paycode', (isset($transaction['paycode']) ? $transaction['paycode'] : '')), 'class'=>'form-control')); ?>
        </div>
    </div>

    <?php // buttons ?>
    <div class="row">
        <a class="btn btn-default" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-success"><span class="fa fa-save"></span> <?php echo lang('core button confirm'); ?></button>
    </div>

<?php echo form_close(); ?>
