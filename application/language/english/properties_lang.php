<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Properties Language File
 */

// Titles
$lang['properties title property_add']              = "Add Property";
$lang['properties title property_delete']           = "Confirm Delete Property";
$lang['properties title property_edit']             = "Edit Property";
$lang['properties title property_list']             = "Property List";
$lang['properties title property_for_sale']         = "%s for Sale%s";
$lang['properties title property_updated_at']       = "Property for Sale%s";

// Buttons
$lang['properties button add_new_property']         = "Add New Property";
$lang['properties button buy']                      = "Buy";
$lang['properties button details']                  = "Details";

// Tooltips
$lang['properties tooltip add_new_property']        = "Add a property to listing.";

// Table Columns
$lang['properties col address']                     = "Address";
$lang['properties col coordinate']                  = "Coordinate";
$lang['properties col is_admin']                    = "Admin";
$lang['properties col owner']                       = "Owner";
$lang['properties col phone']                       = "Phone";
$lang['properties col price']                       = "Price";
$lang['properties col property_id']                 = "ID";
$lang['properties col type']                        = "Type";

// Form Inputs
$lang['properties input address']                   = "Address";
$lang['properties input coordinate']                = "Coordinate";
$lang['properties input description']               = "Description";
$lang['properties input image']                     = "Property Picture";
$lang['properties input phone']                     = "Phone";
$lang['properties input price']                     = "Price";
$lang['properties input status']                    = "Status";
$lang['properties input type']                      = "Type";
$lang['properties input user_id']                   = "Owner ID";

// Messages
$lang['properties msg add_property_success']        = "%s was successfully added!";
$lang['properties msg add_property_warning']       = "%s was successfully added, but with error. %s";
$lang['properties msg delete_confirm']              = "Are you sure you want to delete <strong>%s</strong>? This can not be undone.";
$lang['properties msg delete_property']             = "You have succesfully deleted <strong>%s</strong>!";
$lang['properties msg edit_property_success']       = "%s was successfully modified!";
$lang['properties msg edit_property_warning']       = "%s was successfully modified, but with error. %s";

// Errors
$lang['properties error add_property_failed']       = "%s could not be added! %s";
$lang['properties error delete_property']           = "<strong>%s</strong> could not be deleted!";
$lang['properties error edit_property_failed']      = "%s could not be modified! %s";
$lang['properties error property_id_required']      = "A numeric property ID is required!";
$lang['properties error property_not_exist']        = "That property does not exist!";

// Text
$lang['properties text posted_on_by']               = "%s ago by %s.";
$lang['properties text posted_on_with_detail']      = "Posted %s ago, at %s.";
$lang['properties text property_type']              = "Property Type: %s";