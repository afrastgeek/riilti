<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transaction Language File
 */

$lang['transactions title your_transaction']  = "Your Transaction.";

$lang['transaction msg property_book_success']  = "Property was successfully booked.";

$lang['transaction error property_book_failed']  = "Property could not be booked.";

$lang['transactions input paycode']  = "Payment Code.";