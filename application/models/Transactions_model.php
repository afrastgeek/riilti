<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'transactions';
    }

    /**
     * Get list of non-deleted transactions
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($user_id = '', $limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS $this->_db.*,
            CONCAT(users.first_name, ' ', users.last_name) AS owner
            FROM {$this->_db}
            JOIN users ON users.id = $this->_db.user_id
            WHERE $this->_db.deleted = '0'
        ";

        if ($user_id != '') {
            $sql .= " AND user_id = $user_id";
        }

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Get specific transaction
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_transaction($id = NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT $this->_db.*,
                CONCAT(users.first_name, ' ', users.last_name) AS owner
                FROM {$this->_db}
                JOIN users ON users.id = $this->_db.user_id
                WHERE $this->_db.id = " . $this->db->escape($id) . "
                    AND $this->_db.deleted = '0'
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new transaction
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_transaction($data = array())
    {
        if ($data)
        {
            $sql = "
                INSERT INTO {$this->_db} (
                    user_id,
                    property_id,
                    paycode,
                    status,
                    deleted,
                    created,
                    updated
                ) VALUES (
                    " . $this->db->escape($data['user_id']) . ",
                    " . $this->db->escape($data['property_id']) . ",
                    " . $this->db->escape($data['paycode']) . ",
                    '0',
                    '0',
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "'
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing transaction
     *
     * @param  array $data
     * @return boolean
     */
    function edit_transaction($data = array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                user_id = " . $this->db->escape($data['user_id']) . ",
                property_id = " . $this->db->escape($data['property_id']) . ",
                paycode = " . $this->db->escape($data['paycode']) . ",
                status = " . $this->db->escape($data['status']) . ",
                updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($data['id']) . "
                AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing transaction
     *
     * @param  int $id
     * @return boolean
     */
    function delete_transaction($id = NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    status = '0',
                    deleted = '1',
                    updated = '" . date('Y-m-d H:i:s') . "'
                    WHERE id = " . $this->db->escape($id) . "
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Confirm an existing transaction
     *
     * @param  array $data
     * @return boolean
     */
    function confirm_transaction($data = array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                status = '1',
                updated = '" . date('Y-m-d H:i:s') . "'
                WHERE paycode = " . $this->db->escape($data['paycode']) . "
                AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                // return TRUE;
                $sql = "
                    SELECT property_id
                    FROM $this->_db
                    WHERE paycode = " . $this->db->escape($data['paycode']) . "
                    AND DELETED = '0'
                ";
                return $this->db->query($sql)->result_array()['0']['property_id'];
            }
        }

        return FALSE;
    }
}
