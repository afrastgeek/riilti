<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Properties_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'properties';
    }

    /**
     * Get list of non-deleted properties
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'id', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS $this->_db.*,
            CONCAT(users.first_name, ' ', users.last_name) AS owner
            FROM {$this->_db}
            JOIN users ON users.id = $this->_db.user_id
            WHERE $this->_db.deleted = '0'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Get specific property
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_property($id = NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT $this->_db.*,
                CONCAT(users.first_name, ' ', users.last_name) AS owner
                FROM {$this->_db}
                JOIN users ON users.id = $this->_db.user_id
                WHERE $this->_db.id = " . $this->db->escape($id) . "
                    AND $this->_db.deleted = '0'
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new property
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_property($data = array())
    {
        if ($data)
        {
            $sql = "
                INSERT INTO {$this->_db} (
                    user_id,
                    type,
                    address,
                    coordinate,
                    phone,
                    price,
                    description,
                    picture,
                    status,
                    deleted,
                    created,
                    updated
                ) VALUES (
                    " . $this->db->escape($data['user_id']) . ",
                    " . $this->db->escape($data['type']) . ",
                    " . $this->db->escape($data['address']) . ",
                    " . $this->db->escape($data['coordinate']) . ",
                    " . $this->db->escape($data['phone']) . ",
                    " . $this->db->escape($data['price']) . ",
                    " . $this->db->escape($data['description']) . ",
            ";

            if ($data['picture'] != '') {
                $sql .= "
                    " . $this->db->escape($data['picture']) . ",
                ";
            } else {
                $sql .= "
                    " . $this->db->escape('') . ",
                ";
            }

            $sql .= "
                    " . $this->db->escape($data['status']) . ",
                    '0',
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "'
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing property
     *
     * @param  array $data
     * @return boolean
     */
    function edit_property($data = array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                user_id = " . $this->db->escape($data['user_id']) . ",
                type = " . $this->db->escape($data['type']) . ",
                address = " . $this->db->escape($data['address']) . ",
                coordinate = " . $this->db->escape($data['coordinate']) . ",
                phone = " . $this->db->escape($data['phone']) . ",
                price = " . $this->db->escape($data['price']) . ",
                description = " . $this->db->escape($data['description']) . ",
            ";

            if ($data['picture'] != '') {
                $sql .= "
                    picture = " . $this->db->escape($data['picture']) . ",
                ";
            }

            $sql .= "
                status = " . $this->db->escape($data['status']) . ",
                updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($data['id']) . "
                AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing property
     *
     * @param  int $id
     * @return boolean
     */
    function delete_property($id = NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    status = '0',
                    deleted = '1',
                    updated = '" . date('Y-m-d H:i:s') . "'
                    WHERE id = " . $this->db->escape($id) . "
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * Mark existing property as sold
     *
     * @param  array $data
     * @return boolean
     */
    function mark_property_sold($id)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                status = '0',
                updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($id) . "
                AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }
}
