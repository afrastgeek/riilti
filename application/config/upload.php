<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['upload_path']          = APPPATH . '/../public/uploads/';
$config['allowed_types']        = 'gif|jpg|png';
$config['max_size']             = 2048;
$config['file_ext_tolower']     = TRUE;
$config['encrypt_name']         = TRUE;
$config['overwrite']            = TRUE;
