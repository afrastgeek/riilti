<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('properties');

        // load the properties model
        $this->load->model('properties_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/properties'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "created");
        define('DEFAULT_DIR', "desc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Property list page
     */
    function index()
    {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('user_id'))
        {
            $filters['user_id'] = $this->input->get('user_id', TRUE);
        }

        if ($this->input->get('type'))
        {
            $filters['type'] = $this->input->get('type', TRUE);
        }

        if ($this->input->get('address'))
        {
            $filters['address'] = $this->input->get('address', TRUE);
        }

        if ($this->input->get('price'))
        {
            $filters['price'] = $this->input->get('price', TRUE);
        }

        if ($this->input->get('phone'))
        {
            $filters['phone'] = $this->input->get('phone', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('user_id'))
                {
                    $filter .= "&user_id=" . $this->input->post('user_id', TRUE);
                }

                if ($this->input->post('type'))
                {
                    $filter .= "&type=" . $this->input->post('type', TRUE);
                }

                if ($this->input->post('address'))
                {
                    $filter .= "&address=" . $this->input->post('address', TRUE);
                }

                if ($this->input->post('price'))
                {
                    $filter .= "&price=" . $this->input->post('price', TRUE);
                }

                if ($this->input->post('phone'))
                {
                    $filter .= "&phone=" . $this->input->post('phone', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $properties = $this->properties_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $properties['total'],
            'per_page'   => $limit,
            'attributes' => array('class' => 'page-link')
        ));

        // setup page header data
		$this
            ->add_js_theme( "properties_i18n.js", TRUE )
            ->set_title( lang('properties title property_list') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'properties' => $properties['results'],
            'total'      => $properties['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('admin/properties/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Add new property
     */
    function add()
    {
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('user_id', lang('properties input user_id'), 'required|numeric');
        $this->form_validation->set_rules('address', lang('properties input address'), 'required|trim|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('coordinate', lang('properties input coordinate'), 'trim');
        $this->form_validation->set_rules('phone', lang('properties input phone'), 'required|min_length[3]');
        $this->form_validation->set_rules('price', lang('properties input price'), 'required|numeric');
        $this->form_validation->set_rules('description', lang('properties input description'), 'required|trim|min_length[16]|max_length[255]');
        $this->form_validation->set_rules('status', lang('properties input status'), 'required|numeric');

        if ($this->form_validation->run() == TRUE)
        {
            $data = $this->input->post();

            $this->load->library('upload');
            if ( ! $this->upload->do_upload('picture'))
            {
                $upload_err = $this->upload->display_errors();
                $data['picture'] = '';
            }
            else
            {
                $uploaded = $this->upload->data();
                $data['picture'] = $uploaded['file_name'];
            }

            // save the changes, no matter picture uploaded succesfully or not.
            $saved = $this->properties_model->add_property($data);

            if ($saved)
            {
                if (!isset($upload_err))
                {
                    $this->session->set_flashdata('message', sprintf(lang('properties msg add_property_success'), 'Property'));
                } else {
                    $this->session->set_flashdata('warning', sprintf(lang('properties msg add_property_warning'), 'Property', $upload_err));
                }
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('properties error add_property_failed'), 'Property', $upload_err));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_js_theme( "properties_i18n.js", TRUE )
            ->set_title( lang('properties title property_add') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'property'              => NULL,
        );

        // load views
        $data['content'] = $this->load->view('admin/properties/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Edit existing property
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $property = $this->properties_model->get_property($id);

        // if empty results, return to list
        if ( ! $property)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('user_id', lang('properties input user_id'), 'required|numeric');
        $this->form_validation->set_rules('type', lang('properties input type'), 'required|trim');
        $this->form_validation->set_rules('address', lang('properties input address'), 'required|trim|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('coordinate', lang('properties input coordinate'), 'trim');
        $this->form_validation->set_rules('phone', lang('properties input phone'), 'required|min_length[3]');
        $this->form_validation->set_rules('price', lang('properties input price'), 'required|numeric');
        $this->form_validation->set_rules('description', lang('properties input description'), 'required|trim|min_length[16]|max_length[255]');
        $this->form_validation->set_rules('status', lang('properties input status'), 'required|numeric');

        if ($this->form_validation->run() == TRUE)
        {
            $data = $this->input->post();

            $this->load->library('upload');
            if ( ! $this->upload->do_upload('picture'))
            {
                $upload_err = $this->upload->display_errors();
                $data['picture'] = '';
            }
            else
            {
                $uploaded = $this->upload->data();
                $data['picture'] = $uploaded['file_name'];
            }

            // save the changes, no matter picture uploaded succesfully or not.
            $saved = $this->properties_model->edit_property($data);

            if ($saved)
            {
                if (!isset($upload_err))
                {
                    $this->session->set_flashdata('message', sprintf(lang('properties msg edit_property_success'), 'Property'));
                } else {
                    $this->session->set_flashdata('warning', sprintf(lang('properties msg edit_property_warning'), 'Property', $upload_err));
                }
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('properties error edit_property_failed'), 'Property', $upload_err));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_js_theme( "properties_i18n.js", TRUE )
            ->set_title( lang('properties title property_edit') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'property'              => $property,
            'property_id'           => $id,
        );

        // load views
        $data['content'] = $this->load->view('admin/properties/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    /**
     * Delete a property
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get property details
            $property = $this->properties_model->get_property($id);

            if ($property)
            {
                // soft-delete the property
                $delete = $this->properties_model->delete_property($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('properties msg delete_property'), $property['address']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('properties error delete_property'), $property['address']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('properties error property_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('properties error property_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }


    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('username'))
        {
            $filters['username'] = $this->input->get('username', TRUE);
        }

        if ($this->input->get('first_name'))
        {
            $filters['first_name'] = $this->input->get('first_name', TRUE);
        }

        if ($this->input->get('last_name'))
        {
            $filters['last_name'] = $this->input->get('last_name', TRUE);
        }

        // get all properties
        $properties = $this->properties_model->get_all(0, 0, $filters, $sort, $dir);

        if ($properties['total'] > 0)
        {
            // manipulate the output array
            foreach ($properties['results'] as $key=>$property)
            {
                unset($properties['results'][$key]['password']);
                unset($properties['results'][$key]['deleted']);

                if ($property['status'] == 0)
                {
                    $properties['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $properties['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($properties['results'], "properties");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }
}
