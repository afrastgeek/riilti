<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Properties API
 */
class Properties extends API_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('properties_model');
    }

    /**
     * Default
     * List all property
     */
    public function index()
    {
        // load the properties model and admin language file
        $this->lang->load('admin');

        // get user data
        $properties = $this->properties_model->get_all();
        $results['data'] = NULL;

        if ($properties)
        {
            // build usable array
            foreach($properties['results'] as $property)
            {
                $results['data'][$property['id']] = array(
                    'owner'         => $property['owner'],
                    'user_id'       => $property['user_id'],
                    'address'       => $property['address'],
                    'coordinate'    => $property['coordinate'],
                    'phone'         => $property['phone'],
                    'price'         => $property['price'],
                    'description'   => $property['description'],
                    'status'        => ($property['status']) ? lang('admin input active') : lang('admin input inactive'),
                    'created'       => $property['created'],
                    'updated'       => $property['updated']
                );
            }
            $results['total'] = $properties['total'];
        }
        else
            $results['error'] = lang('core error no_results');

        // display results using the JSON formatter helper
        display_json($results);
        exit;
    }

    /**
     * Sold
     */
    public function sold()
    {
        // load the properties model and admin language file
        $this->lang->load('admin');

        // get user data
        $properties = $this->properties_model->get_all('', '', array('properties.status' => 0));
        $results['data'] = NULL;

        if ($properties)
        {
            // build usable array
            foreach($properties['results'] as $property)
            {
                $results['data'][$property['id']] = array(
                    'owner'         => $property['owner'],
                    'user_id'       => $property['user_id'],
                    'address'       => $property['address'],
                    'coordinate'    => $property['coordinate'],
                    'phone'         => $property['phone'],
                    'price'         => $property['price'],
                    'description'   => $property['description'],
                    'status'        => ($property['status']) ? lang('admin input active') : lang('admin input sold'),
                    'created'       => $property['created'],
                    'updated'       => $property['updated']
                );
            }
            $results['total'] = $properties['total'];
        }
        else
            $results['error'] = lang('core error no_results');

        // display results using the JSON formatter helper
        display_json($results);
        exit;
    }
}