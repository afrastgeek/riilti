<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('transactions');

        // load the transactions model
        $this->load->model('transactions_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('transaction'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit - 1);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "created");
        define('DEFAULT_DIR', "desc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Default
     */
    function index() {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('property_id'))
        {
            $filters['property_id'] = $this->input->get('property_id', TRUE);
        }

        if ($this->input->get('paycode'))
        {
            $filters['paycode'] = $this->input->get('paycode', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('property_id'))
                {
                    $filter .= "&property_id=" . $this->input->post('property_id', TRUE);
                }

                if ($this->input->post('payment_code'))
                {
                    $filter .= "&payment_code=" . $this->input->post('payment_code', TRUE);
                }

                if ($this->input->post('status'))
                {
                    $filter .= "&status=" . $this->input->post('status', TRUE);
                }

                if ($this->input->post('updated'))
                {
                    $filter .= "&updated=" . $this->input->post('updated', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get user id
        $user_id = $this->session->userdata('logged_in')['id'];

        // get list
        $transactions = $this->transactions_model->get_all($user_id, $limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $transactions['total'],
            'per_page'   => $limit,
            'attributes' => array('class' => 'page-link')
        ));

        // setup page header data
        $this
            ->add_js_theme( "transactions_i18n.js", TRUE )
            ->set_title(lang('transactions title your_transaction'));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'transactions' => $transactions['results'],
            'total'      => $transactions['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('transactions/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Add new property
     */
    function add($property_id)
    {
        $data = array(
            'user_id'=>$this->session->userdata('logged_in')['id'],
            'property_id'=>$property_id,
            'paycode'=>substr(uniqid('', true), -5)
        );

        $saved = $this->transactions_model->add_transaction($data);

        if ($saved)
        {
            $this->session->set_flashdata('message', lang('transaction msg property_book_success'));
        }
        else
        {
            $this->session->set_flashdata('error', lang('transaction error property_book_failed'));
        }

        // return to list and display message
        redirect(THIS_URL);
    }
}
