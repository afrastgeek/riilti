<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends Public_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('properties');

        // load the properties model
        $this->load->model('properties_model');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('property'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit - 1);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "created");
        define('DEFAULT_DIR', "desc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Default
     */
    function index() {
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('user_id'))
        {
            $filters['user_id'] = $this->input->get('user_id', TRUE);
        }

        if ($this->input->get('type'))
        {
            $filters['type'] = $this->input->get('type', TRUE);
        }

        if ($this->input->get('address'))
        {
            $filters['address'] = $this->input->get('address', TRUE);
        }

        if ($this->input->get('price'))
        {
            $filters['price'] = $this->input->get('price', TRUE);
        }

        if ($this->input->get('phone'))
        {
            $filters['phone'] = $this->input->get('phone', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('user_id'))
                {
                    $filter .= "&user_id=" . $this->input->post('user_id', TRUE);
                }

                if ($this->input->post('type'))
                {
                    $filter .= "&type=" . $this->input->post('type', TRUE);
                }

                if ($this->input->post('address'))
                {
                    $filter .= "&address=" . $this->input->post('address', TRUE);
                }

                if ($this->input->post('price'))
                {
                    $filter .= "&price=" . $this->input->post('price', TRUE);
                }

                if ($this->input->post('phone'))
                {
                    $filter .= "&phone=" . $this->input->post('phone', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $properties = $this->properties_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $properties['total'],
            'per_page'   => $limit,
            'attributes' => array('class' => 'page-link')
        ));

        // setup page header data
        $this
            ->add_js_theme( "properties_i18n.js", TRUE )
            ->set_title( sprintf(lang('properties title property_for_sale'),'Property', '') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'properties' => $properties['results'],
            'total'      => $properties['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir
        );

        // load views
        $data['content'] = $this->load->view('properties/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function detail($id) {
        $property = $this->properties_model->get_property($id);

        // setup page header data
        $this->set_title( sprintf(lang('properties title property_for_sale'), ucfirst($property['type']), ' by ' . $property['owner']) );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'property'  => $property,
        );

        // load views
        $data['content'] = $this->load->view('properties/detail', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}
