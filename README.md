# riilti

<a name="toc"></a>
## TABLE OF CONTENTS
* [Introduction](#introduction)
* [System Requirements](#system-requirements)
* [Installation](#installation)
* [What's New](#whats-new)

<a name="introduction"></a>
## INTRODUCTION

riilti is built upon [Code Igniter](https://www.codeigniter.com/) based on
[CI3 Fire Starter](https://github.com/JasonBaier/ci3-fire-starter).

<a name="system-requirements"></a>
## SYSTEM REQUIREMENTS

* PHP version 5.6+ (successfully tested on PHP 7.1.x)
* MySQL 5.1+
* PHP GD extension for CAPTCHA to work
* PHP Mcrypt extension if you want to use the Encryption class

See CodeIgniter's [Server Requirements](https://codeigniter.com/user_guide/general/requirements.html)
for the complete list.

<a name="installation"></a>
## INSTALLATION

* Create a new database and import the included sql file from the /data folder
    + default administrator username/password is **admin/admin**
* Modify the /application/config/config.php
    + line 26: set your base site URL, we use `riilti.dev` for development.
    + line 220: set your log threshold - I usually set it to 1 for production environments
    + line 314: set your encryption key using the [recommended method](http://www.codeigniter.com/user_guide/libraries/encryption.html#setting-your-encryption-key "Encryption Library: Setting your encryption key")
* Modify /application/config/database.php and connect to your database
* Modify /application/config/core.php and set $config['root_folder'] to match your server's webroot (htdocs, public_html, etc.)
* Upload all files to your server - for security, /application and /system must go above your webroot and all the files and subfolders in /publics will go inside your webroot
* Make sure the /captcha folder inside your webroot has write permission
* Set /application/sessions permission to 0600
* Visit your new URL
* Make sure you log in to admin and change the administrator password!

<a name="whats-new"></a>
## WHAT'S NEW

#### Version 1.0.0
27/05/2017

* Initial version