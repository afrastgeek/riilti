/* Custom File Input */
$('.custom-file-input').on('change',function(){
  $(this).next('.custom-file-control').addClass("selected").html($(this).val());
})

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#selected-image').attr('src', e.target.result);
            $('#selected-image').toggleClass('hidden-xs-up');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".custom-file-input").change(function(){
    readURL(this);
});