$(document).ready(function() {

    /**
     * Delete a property
     */
    $('.btn-delete-property').click(function() {
        window.location.href = "/admin/properties/delete/" + $(this).attr('data-id');
    });

});
