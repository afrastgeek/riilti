<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
	<link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>

    <?php // CSS files ?>
    <?php if (isset($css_files) && is_array($css_files)) : ?>
        <?php foreach ($css_files as $css) : ?>
            <?php if ( ! is_null($css)) : ?>
                <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <?php // Fixed navbar ?>
    <nav class="navbar navbar-toggleable-md navbar navbar-inverse bg-inverse">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><?php echo $this->settings->site_name; ?></a>
            <div id="navbar" class="navbar-collapse collapse">
                <?php // Nav bar left ?>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?php echo (uri_string() == 'admin' OR uri_string() == 'admin/dashboard') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin'); ?>" class="nav-link"><?php echo lang('admin button dashboard'); ?></a></li>
                    <li class="nav-item dropdown<?php echo (strstr(uri_string(), 'admin/properties')) ? ' active' : ''; ?>">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo lang('admin button properties'); ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="<?php echo (uri_string() == 'admin/properties') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/properties'); ?>" class="dropdown-item"><?php echo lang('admin button properties_list'); ?></a></li>
                            <li class="<?php echo (uri_string() == 'admin/properties/add') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/properties/add'); ?>" class="dropdown-item"><?php echo lang('admin button properties_add'); ?></a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown<?php echo (strstr(uri_string(), 'admin/users')) ? ' active' : ''; ?>">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo lang('admin button users'); ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="<?php echo (uri_string() == 'admin/users') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/users'); ?>" class="dropdown-item"><?php echo lang('admin button users_list'); ?></a></li>
                            <li class="<?php echo (uri_string() == 'admin/users/add') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/users/add'); ?>" class="dropdown-item"><?php echo lang('admin button users_add'); ?></a></li>
                        </ul>
                    </li>
                    <li class="nav-item <?php echo (uri_string() == 'admin/contact') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/contact'); ?>" class="nav-link"><?php echo lang('admin button messages'); ?></a></li>
                    <li class="nav-item <?php echo (uri_string() == 'admin/settings') ? 'active' : ''; ?>"><a href="<?php echo base_url('/admin/settings'); ?>" class="nav-link"><?php echo lang('admin button settings'); ?></a></li>
                </ul>
                <?php // Nav bar right ?>
                <ul class="navbar-nav">
                    <li class="nav-item"><a href="<?php echo base_url('logout'); ?>" class="nav-link"><?php echo lang('core button logout'); ?></a></li>
                    <li class="nav-item dropdown">
                        <button id="session-language" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle">
                            <i class="fa fa-language"></i>
                            <span class="caret"></span>
                        </button>
                        <ul id="session-language-dropdown" class="dropdown-menu" role="menu" aria-labelledby="session-language">
                            <?php foreach ($this->languages as $key=>$name) : ?>
                                <li>
                                    <a href="#" rel="<?php echo $key; ?>" class="dropdown-item">
                                        <?php if ($key == $this->session->language) : ?>
                                            <i class="fa fa-check selected-session-language"></i>
                                        <?php endif; ?>
                                        <?php echo $name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php // Main body ?>
    <div class="container pb-5" role="main">

        <?php // Page title ?>
        <div class="mt-4 mb-5">
            <h1><?php echo $page_header; ?></h1>
        </div>

        <?php // System messages ?>
        <?php if ($this->session->flashdata('message')) : ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php elseif ($this->session->flashdata('warning')) : ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('warning'); ?>
            </div>
        <?php elseif (validation_errors()) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
            </div>
        <?php elseif ($this->error) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->error; ?>
            </div>
        <?php endif; ?>

        <?php // Main content ?>
        <?php echo $content; ?>

    </div>

    <?php // Footer ?>
    <footer class="sticky-footer">
        <div class="container">
            <p class="text-muted">
                <?php echo lang('core text page_rendered'); ?>
                | PHP v<?php echo phpversion(); ?>
                | CodeIgniter v<?php echo CI_VERSION; ?>
                | <?php echo $this->settings->site_name; ?> v<?php echo $this->settings->site_version; ?>
                | <a href="https://gitlab.com/afrastgeek/riilti" target="_blank">Gitlab.com</a>
            </p>
        </div>
    </footer>

    <?php // Javascript files ?>
    <?php if (isset($js_files) && is_array($js_files)) : ?>
        <?php foreach ($js_files as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
        <?php foreach ($js_files_i18n as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript"><?php echo "\n" . $js . "\n"; ?></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

</body>
</html>
