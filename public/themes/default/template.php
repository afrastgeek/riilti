<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Default Public Template
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>
    <meta name="keywords" content="<?php echo $this->settings->meta_keywords; ?>">
    <meta name="description" content="<?php echo $this->settings->meta_description; ?>">

    <?php // CSS files ?>
    <?php if (isset($css_files) && is_array($css_files)) : ?>
        <?php foreach ($css_files as $css) : ?>
            <?php if ( ! is_null($css)) : ?>
                <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <?php // Fixed navbar ?>
    <nav class="px-5 navbar sticky-top navbar-toggleable-md navbar-light<?php if ($this->session->userdata('logged_in')) { echo ' bg-white shadow-1' ; } ?>">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand text-warning font-weight-bold" href="<?php echo base_url('/'); ?>"><?php echo $this->settings->site_name; ?></a>
            <div id="navbar" class="navbar-collapse collapse">
                <?php // Nav bar left ?>
                <ul class="navbar-nav mr-auto">
                    <?php if ($this->session->userdata('logged_in')) : ?>
                        <li class="nav-item<?php echo (uri_string() == 'property') ? ' active' : ''; ?>"><a href="<?php echo base_url('/property'); ?>" class="nav-link"><?php echo lang('core button property'); ?></a></li>
                        <li class="nav-item<?php echo (uri_string() == 'transaction') ? ' active' : ''; ?>"><a href="<?php echo base_url('/transaction'); ?>" class="nav-link"><?php echo lang('core button transaction'); ?></a></li>
                    <?php endif; ?>
                </ul>
                <?php // Nav bar right ?>
                <ul class="navbar-nav">
                    <?php if ($this->session->userdata('logged_in')) : ?>
                        <?php if ($this->user['is_admin']) : ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin'); ?>" class="nav-link"><?php echo lang('core button admin'); ?></a>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item<?php echo (uri_string() == 'profile') ? ' active' : ''; ?>"><a href="<?php echo base_url('/profile'); ?>" class="nav-link"><?php echo lang('core button profile'); ?></a></li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('logout'); ?>" class="nav-link"><?php echo lang('core button logout'); ?></a>
                        </li>
                    <?php else : ?>
                        <li class="nav-item<?php echo (uri_string() == 'login') ? ' active' : ''; ?>">
                            <a href="<?php echo base_url('login'); ?>" class="nav-link"><?php echo lang('core button login'); ?></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
    </nav>

    <?php // Main body ?>
    <div class="container theme-showcase" role="main">

        <?php // Page title ?>
        <div class="mt-5 mb-5">
            <h1><?php echo $page_header; ?></h1>
        </div>

        <?php // System messages ?>
        <?php if ($this->session->flashdata('message')) : ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php elseif (validation_errors()) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
            </div>
        <?php elseif ($this->error) : ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->error; ?>
            </div>
        <?php endif; ?>

        <?php // Main content ?>
        <?php echo $content; ?>

    </div>

    <?php // Footer ?>
    <footer class="mt-5 bt b--light-gray">
        <div class="container">
            <div class="row my-5 ">
                <div class="col-3">
                    <div class="dropdown">
                        <button id="session-language" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-secondary dropdown-toggle text-capitalize">
                            <?php echo $this->session->language ?>
                            <span class="caret"></span>
                        </button>
                        <ul id="session-language-dropdown" class="dropdown-menu" role="menu" aria-labelledby="session-language">
                            <?php foreach ($this->languages as $key=>$name) : ?>
                                <li>
                                    <a href="#" rel="<?php echo $key; ?>" class="dropdown-item">
                                        <?php if ($key == $this->session->language) : ?>
                                            <i class="fa fa-check selected-session-language"></i>
                                        <?php endif; ?>
                                        <?php echo $name; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        </div>
                </div>
                <div class="col-3">
                    <h4><?php echo $this->settings->site_name; ?></h4>
                    <ul class="list-unstyled mb-0">
                        <li><a href="<?php echo base_url('/about'); ?>"><?php echo lang('core text about_us'); ?></a></li>
                        <li><a href="<?php echo base_url('/contact'); ?>"><?php echo lang('core button contact'); ?></a></li>
                    </ul>
                </div>
                <div class="col-3"></div>
                <div class="col-3"></div>
            </div>
            <div class="clearfix"><hr /></div>
            <span class="text-danger font-weight-bold">riilti</span> © 2017.
            <span class="text-muted pull-right">
                <?php echo lang('core text page_rendered'); ?>
                | PHP v<?php echo phpversion(); ?>
                | CodeIgniter v<?php echo CI_VERSION; ?>
                | <?php echo $this->settings->site_name; ?> v<?php echo $this->settings->site_version; ?>
                | <a href="https://gitlab.com/afrastgeek/riilti" target="_blank">Gitlab.com</a>
            </span>
        </div>
    </footer>

    <?php // Javascript files ?>
    <?php if (isset($js_files) && is_array($js_files)) : ?>
        <?php foreach ($js_files as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
        <?php foreach ($js_files_i18n as $js) : ?>
            <?php if ( ! is_null($js)) : ?>
                <?php echo "\n"; ?><script type="text/javascript"><?php echo "\n" . $js . "\n"; ?></script><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

</body>
</html>
